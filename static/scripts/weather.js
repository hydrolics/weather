(function(window, document, $) {
  var Weather = {
    URLS: {
      'btn-hourly-weather': '/hourly/',
      'btn-detailed-weather': 'detailed-weather.html'
    },

    init: function() {
      // Buttons
      $('.btn-hourly-weather').bind('click', Weather.btnHandler );
      $('.btn-detailed-weather').bind('click', Weather.btnHandler );
    },

    btnHandler: function(e) {
      var btn = $(e.target).closest('div');
      var url = Weather.URLS[btn.attr('class')];
      Weather.slideTo(url);
    },

    slideTo: function(url) {
      var content = $('#content');
      var margin = parseInt(content.css('margin-left').replace('px', ''));
      var width = content.width();
      var height = content.height();

      // Set overflow to hide new content;
      //content.css('overflow', 'hidden');
      
      // Add class with transition
      content.addClass('slide');

      // Create a temp div to hold old and new content
      var tempContent = $('<div/>', {
        id: 'temp-content',
        width: width * 2,
        height: height
      });

      // Create temp div for old content
      var oldContent = $('<div/>', { id: 'temp-old', width: width });
      oldContent.append($('#content').html());
      oldContent.css('display', 'inline-block');

      // Create temp div for new content
      var newContent = $('<div/>', { id: 'temp-new', width: width });
      newContent.css('display', 'inline-block');

      // Load new content
      newContent.load(url, function() {

        // Place old and new content next to each other
        oldContent.css('float', 'left');
        newContent.css('float', 'left');

        // Add content to temp div
        tempContent.append(oldContent);
        tempContent.append(newContent);

        // Replace original with tempContent
        content.html(tempContent);

        // Animate to new content
        content.css('margin-left', (width * -1) + margin);

        // Replace original content with new content
        // TODO: support all transitions
        content.bind('webkitTransitionEnd', function() {
          content.removeClass('slide');
          content.css('margin', 'auto');
          content.html(newContent.children('div'));
          content.unbind('webkitTransitionEnd');
        });
      });

    },

  };

  window.Weather = Weather.init;

})(window, document, $);
