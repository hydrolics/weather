import urllib2
import simplejson

URL = 'http://api.wunderground.com/api/'

class QueryNotFoundError(Exception): pass
class ServerError(Exception): pass

class Underground(object):
  
    def __init__(self, key):
        self.key = key

    def get_feature_by_ip(self, feature, ip_address):
        query = 'autoip.json?geo_ip=%s' % (ip_address)
        return self._request(feature, query)

    def _request(self, features, query):
        uri = '%(url)s/%(key)s/%(features)s/q/%(query)s' % {
          'url': URL,
          'key': self.key,
          'features': features,
          'query': query,
          #'format': 'json'
        }
        print uri

        response = urllib2.urlopen(uri)
        to_json = response.read()
        json = simplejson.loads(to_json)

        if 'error' in json['response']:
            error_type = json['response']['error']['type']
            error_description = json['response']['error']['description']

            if error_type == "querynotfound":
                raise QueryNotFoundError(error_description)
            else:
                raise ServerError(error_description)

        else:
            return json


if __name__ == '__main__':
    underground = Underground('6658ea5e8eb9ea20')
    print underground.get_feature_by_ip('conditions', '98.118.119.64')
