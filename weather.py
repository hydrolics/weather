from flask import Flask
from flask import render_template, request

import os

from lib.underground import Underground, QueryNotFoundError


app = Flask(__name__)

UNDERGROUND_KEY = '6658ea5e8eb9ea20' 

@app.route('/')
@app.route('/current/')
def index():
    ''' Index page '''
    print request.remote_addr
    underground = Underground(UNDERGROUND_KEY)

    try:
        conditions = underground.get_feature_by_ip('conditions',
                                                   request.access_route[0]) 
    except QueryNotFoundError:
        if app.debug:
            conditions = underground.get_feature_by_ip('conditions',
                                                       '98.118.119.64') 
        

    return render_template('current-weather.html', conditions=conditions)

@app.route('/hourly/')
def hourly():
    ''' Hourly weather data '''
    if request.is_xhr:
        full = False
    else:
        full = True

    underground = Underground(UNDERGROUND_KEY)

    try:
        hourly = underground.get_feature_by_ip('hourly',
                                               request.access_route[0]) 
    except QueryNotFoundError:
        if app.debug:
            hourly = underground.get_feature_by_ip('hourly',
                                                   '98.118.119.64') 

    return render_template('hourly-weather.html', full=full, hourly=hourly )

if __name__ == '__main__':
    PORT = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=PORT, debug=True)
